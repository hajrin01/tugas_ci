-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 08 Mei 2019 pada 06.05
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stok`) VALUES
('BR001', 'PC', 2000000, 'JS002', 1, -7),
('BR002', 'Meja', 100000, 'JS001', 1, -100002),
('BR003', 'buku', 20000, 'JS001', 1, 3),
('BR004', 'Pensil', 5000, 'JS001', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('JB002', 'Marketing', 'Operasional', 1),
('JB003', 'IT Suport', 'Operasional', 1),
('JB005', 'Manager', 'Operasional', 1),
('JB009', 'Direktur', 'Manager', 1),
('JB010', 'Manager', 'Operasional', 1),
('JB011', 'Obe', 'Manager', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JS001', 'Alat Tulis kantor', 1),
('JS002', 'Perangkat lunak', 1),
('JS003', 'Perangkat lunak', 1),
('JS004', 'Alat Tulis kantor', 1),
('JS006', 'Perangkat lunak', 1),
('JS007', 'Alat Tulis kantor', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`, `photo`) VALUES
('1904001', 'Muhrin Hajrin', 'Kampung Baru', '2019-04-16', 'p', 'jlkkkkck', '082359150340', 'JB002', 1, '20190419_1904001.jpg'),
('1904002', 'Furkan hajrin', 'Kampung Baru', '2019-04-17', 'p', '                  jl. Marunda', '082359150340', 'JB002', 1, '20190418_1904002.jpg'),
('1904003', 'Putri Arini Hajrin', 'Ende Utara', '1999-04-06', 'p', '                  jl.jl,mmm', '082359150340', 'JB003', 1, '20190419_1904003.jpg'),
('1904005', 'Akila Pranaja', 'Jakrta Utara', '2019-04-10', 'l', '                  jkljklnl', '89790890', 'JB002', 1, '20190422_1904005.jpg'),
('1905006', 'Abdul Bahar', 'Jakarta', '1998-02-12', 'L', 'Jl. Plumpang Semper', '082389890234', 'JB003', 1, '20190505_1905006.jpg'),
('1905007', 'Murni Wati', 'Ende', '2000-01-22', 'p', '                  Jl.Sudirman', '081245679098', 'JB002', 1, '20190505_1905007.jpg'),
('1905008', 'Sahril Ramadhan', 'Kampung Baru', '1996-09-18', 'L', 'Jl. Eltari', '089967087880', 'JB002', 1, '20190505_1905008.jpg'),
('1905009', 'Muhammad Safi''in', 'Ende', '1996-04-20', 'L', 'Jl. Ahmad Yani', '081678908235', 'JB003', 1, '20190505_1905009.jpg'),
('1905010', 'Iwan Usman', 'Metinumba', '1997-04-12', 'L', 'Jl. Gatot Subroto', '082209082983', 'JB009', 1, '20190505_1905010.jpg'),
('1905011', 'Putri Elfi', 'Ende', '1997-03-09', 'L', 'Jl. Kelimutu', '082234587690', 'JB002', 1, '20190505_1905011.jpg'),
('1905012', 'Putri Asdian', 'Ende', '1998-09-18', 'P', 'Jl. Wirajaya', '081278907234', 'JB009', 1, 'default.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(18, 20, 'BR002', 2, 5000000, 10000000, 1),
(20, 21, 'BR001', 2, 12000000, 24000000, 1),
(21, 21, 'BR003', 3, 15000000, 45000000, 1),
(22, 21, 'BR003', 6, 80000, 480000, 1),
(23, 21, 'BR004', 4, 80000, 320000, 1),
(24, 21, 'AS09', 10, 600, 6000, 1),
(25, 21, 'BR004', 7, 20000, 140000, 1),
(26, 22, 'BR001', 7, 500000, 3500000, 1),
(27, 22, 'BR003', 7, 20000000, 140000000, 1),
(28, 22, 'BR003', 7, 20000000, 140000000, 1),
(29, 34, 'BR003', 12, 50000000, 600000000, 1),
(30, 35, 'BR001', 9, 200000, 1800000, 1),
(31, 36, 'JP010', 7, 20000000, 140000000, 1),
(32, 32, 'AS09', 7, 200000, 1400000, 1),
(33, 32, 'JP001', 4, 200000, 800000, 1),
(34, 37, 'AS09', 12, 50000, 600000, 1),
(35, 23, 'BR004', 2, 10000, 20000, 1),
(36, 23, 'BR001', 5, 50000, 250000, 1),
(37, 21, 'BR001', 4, 60000, 240000, 1),
(38, 21, 'BR002', 9, 33434, 300906, 1),
(39, 41, 'BR004', 1, 20000, 20000, 1),
(40, 47, 'BR004', 1, 30000, 30000, 1),
(41, 55, 'BR001', 6, 500000, 3000000, 1),
(42, 55, 'BR004', 9, 90000, 810000, 1),
(43, 56, 'BR001', 9, 4000000, 36000000, 1),
(44, 56, 'BR006', 1, 200000, 200000, 1),
(45, 55, 'BR001', 1, 400000, 400000, 1),
(46, 57, 'BR003', 2, 10000, 20000, 1),
(47, 57, 'BR004', 3, 10000, 30000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` varchar(11) NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `tanggal` date NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `kode_supplier`, `tanggal`, `approved`, `flag`) VALUES
(55, 'TR90409B001', 'SP004', '2019-04-22', 1, 1),
(56, 'TR90409B905', 'SP004', '2019-04-22', 1, 1),
(57, 'TR90404A905', 'SP004', '2019-04-23', 1, 1),
(58, 'TR90506A905', 'SP004', '2019-05-05', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id_jual_d`, `id_jual_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 12346, 'AS09', 12, 20000000, 20000000, 1),
(2, 12345, 'BR001', 4, 8000, 32000, 1),
(3, 12345, 'BR001', 6, 8000, 48000, 1),
(4, 12345, 'BR002', 2, 5000000, 10000000, 1),
(5, 12345, 'BR006', 3, 2000000, 6000000, 1),
(6, 12345, 'BR001', 1, 2000000, 2000000, 1),
(7, 12351, 'BR001', 2, 2000000, 4000000, 1),
(8, 12352, 'BR004', 3, 5000, 15000, 1),
(9, 12351, 'BR002', 1, 100000, 100000, 1),
(10, 12352, 'BR001', 1, 2000000, 2000000, 1),
(11, 12353, 'BR002', 100000, 100000, 10000000000, 1),
(12, 12354, 'BR003', 3, 20000, 60000, 1),
(13, 12354, 'BR002', 2, 100000, 200000, 1),
(14, 12356, 'BR001', 4, 2000000, 8000000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `pembeli` varchar(250) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan_header`
--

INSERT INTO `penjualan_header` (`id_jual_h`, `no_transaksi`, `tanggal`, `pembeli`, `flag`) VALUES
(12351, 'TR90416A001', '2019-04-28', 'Fara', 1),
(12352, 'TR90416A905', '2019-04-28', 'murni', 1),
(12354, 'TR90513B905', '2019-05-02', 'muhrin', 1),
(12355, 'TR90515B906', '2019-05-02', 'Fara', 1),
(12356, 'TR90515B906', '2019-05-02', 'murni', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP004', 'Furkan', 'Jl. Swasmbada No. 17', '081234567890', 1),
('SP005', 'Muhrin', 'Jl. waraks', '081903839', 1),
('SP006', 'Rais', 'Jl. Sungai Bambu', '0827289393', 1),
('SP007', 'Murni Wati', 'Jl. swasmbada', '081263783', 1),
('SP008', 'Salsa', 'Jl. Plumpang Raya', '0812373838', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, 'Admin', 'muhrinhajrin02@gmail.com', 'c93ccd78b2076528346216b3b2f701e6', 1, 1),
(2, '1704421300', 'rinhajrin0@gmail.com', 'cc63027830c1a0db0c6d6b69e91dde3b', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12357;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
