<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Penjualan_model extends CI_Model
{
    //panggil nama table
    private $_table_header = "penjualan_header";
    private $_table_detail = "penjualan_detail";


    public function tampilDataPenjualan()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
        );
        return $query->result();	
    }


    public function savePenjualanHeader()
    {
        $data['no_transaksi']   = $this->input->post('no_transaksi');
        $data['tanggal']        = date('Y-m-d');
        $data['pembeli']  = $this->input->post('pembeli');
        $data['flag']           = 1;

        $this->db->insert($this->_table_header, $data);
    }



    public function idTransaksiTerakhir()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_jual_h DESC LIMIT 0,1"
        );
        $data_id = $query->result();

        foreach ($data_id as $data) {
            $last_id = $data->id_jual_h;
        }

        return $last_id;
    }

    public function tampilDataPenjualanDetail($id)
    {
        $query	= $this->db->query(
            "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_jual_h = '$id'"
        );
        
        return $query->result();	
    }


public function tampilLaporanPenjualan2($tgl_awal,$tgl_akhir)
    {
       $this->db->select("ph.id_jual_h, ph.no_transaksi,ph.tanggal, COUNT(pd.kode_barang) AS total_barang, SUM(pd.qty) AS total_qty, SUM(pd.jumlah) AS total_penjualan");
      $this->db->FROM("penjualan_header AS ph"); 
       $this->db->JOIN("penjualan_detail AS pd", "ph.id_jual_h = pd.id_jual_h");
       $this->db->WHERE("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
       $this->db->GROUP_BY("ph.id_jual_h");
        $query = $this->db->get();
         
        return $query->result();   
    }


    public function savePenjualanDetail($id)
    {   
        
        $qty    = $this->input->post('qty');
        $harga  = $this->input->post('harga');
        $kode_barang = $this->input->post('kode_barang');
        $harga_barang  = $this->barang_model->cariHargaBarang($kode_barang);

        $data['id_jual_h'] = $id;
        $data['kode_barang']    = $kode_barang;
        $data['qty']            = $qty;
        $data['harga']          = $harga_barang;
        $data['jumlah']         = $qty * $harga_barang;
        $data['flag']           = 1;

        $this->db->insert($this->_table_detail, $data);
    }
	

public function createKodeUrut() {
        $this->db->select('MAX(no_transaksi) as no_transaksi');
        $query = $this->db->get($this->_table_header);
        $result = $query->row_array();
        
        $no_transaksi_terakhir = $result['no_transaksi'];
        $label = "TR";
        $labelthn = substr(date('y'), 1,1 );
        $labelbln = date('m');
        $labeljam = date('H');
        $rubahjam = "";
        if($labeljam % 2 ==0) {
            $rubahjam = "A";
        }else {
            $rubahjam = "B";
        }
        $no_transaksi_lama = (int) substr($no_transaksi_terakhir, 2, 3);
        $no_transaksi_lama ++;
        
        $no_transaksi_baru = sprintf("%03s", $no_transaksi_lama);
        $no_transaksi_baru = $label . $labelthn .  $labelbln .  $labeljam . $rubahjam .  $no_transaksi_baru;
        
        return $no_transaksi_baru;
}

   public function rules()
{
        return[
        [
                'field' =>'no_transaksi',
                'label' =>'No Transaksi',
                'rules' =>'required|max_length[20]',
                'errors' =>[
                    'required' => 'No Transaksi tidak boleh kosong.',
                    'max_length' => 'No Transaksi tidak boleh lebih dari 10 karakter.',
                ]

        ],
        [
                'field' =>'pembeli',
                'label' =>'Nama Pembeli',
                'rules' =>'required',
                'errors' =>[
                    'required' => 'Nama Pembeli tidak boleh kosong.',
                  
                ]

        ]
        ];
    }
    


 public function rules1()
{
        return[
        [
                'field' =>'kode_barang',
                'label' =>'Kode Barang',
                'rules' =>'required|max_length[5]',
                'errors' =>[
                    'required' => 'Kode barang tidak boleh kosong.',
                    'max_length' => 'Kode barang harus angka.',  
                ]
        
        ],
        [
                'field' =>'qty',
                'label' =>'Qty',
                'rules' =>'required|numeric',
                'errors' =>[
                    'required' => 'Qty tidak boleh kosong.',
                    'numeric' => 'Qty barang harus angka.',
                    

                 ]

         ]
         ];
     }
    

    public function tampilDataPenjualanPagination($perpage, $uri, $data_pencarian)
{
    $this->db->select('*');
    if (!empty($data_pencarian)) {
        $this->db->like('pembeli', $data_pencarian);
        }
        $this->db->order_by('id_jual_h','asc');
        
        $get_data = $this->db->get($this->_table_header, $perpage, $uri);
        if ($get_data->num_rows() > 0) {
            return $get_data->result();
            
            }else{
                return null;
            }   
}


public function tombolpagination($data_pencarian)
{
    $this->db->like('pembeli', $data_pencarian);
    $this->db->from($this->_table_header);
    $hasil = $this->db->count_all_results();
    
    //pagination limt
    $pagination['base_url'] = base_url().'penjualan/listpenjualan/load/';
    $pagination['total_rows'] =$hasil;
    $pagination['per_page'] = "3";
    $pagination['uri_segment'] = 4;
    $pagination['num_links'] = 2;
    
    
    $pagination['full_tag_open'] = '<div class="pagination">';
    $pagination['full_tag_close'] = '</div>';
    
    $pagination['first_link'] = '&nbsp;&nbsp;<button type="button" 
                                class="btn btn-info">First</button>';
    $pagination['first_tag_open'] = '<span class="firstlink">';
    $pagination['first_tag_close'] = '</span>&nbsp;<&nbsp;';
    
    
    $pagination['last_link'] = '&nbsp;&nbsp;>&nbsp;<button type="button" 
                                class="btn btn-info">Last</button>';
    $pagination['last_tag_open'] = '<span class="lastlink">';
    $pagination['last_tag_close'] = '</span>';
    
    $pagination['next_link'] = '&nbsp;&nbsp;<button type="button" class="btn btn-info">
                                Next</button>';
    $pagination['next_tag_open'] = '<span class="nextlink">';
    $pagination['next_tag_close'] = '</span>';
    
    
    $pagination['prev_link'] = '<button type="button" class="btn btn-info">
                                Prev</button>';
    $pagination['prev_tag_open'] = '<span class="prevlink">';
    $pagination['prev_tag_close'] = '</span>&nbsp;&nbsp;';
    
    
    $pagination['cur_tag_open'] = '<span class="curlink" style="color: red">';
    $pagination['cur_tag_close'] = '</span>&nbsp;&nbsp;';
    
    $pagination['num_tag_open'] = '<span class="numlink">';
    $pagination['num_tag_close'] = '&nbsp;&nbsp;</span>';
    
    $this->pagination->initialize($pagination);
    
    $hasil_pagination = $this->tampilDataPenjualanPagination($pagination['per_page'],
    $this->uri->segment(4), $data_pencarian);
    
    return $hasil_pagination;
    
    }



}