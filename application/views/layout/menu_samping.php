<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url();?>assets/admin-lte/dist/img/rin.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Muhrin Hajrin</p>
          <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
  
      <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>DATA MASTER</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-circle-o"></i>Data Karyawan</a></li>
            <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-circle-o"></i>Data Jabatan</a></li>
            <li><a href="<?=base_url();?>barang/listbarang"><i class="fa fa-circle-o"></i>Data Barang</a></li>
            <li><a href="<?=base_url();?>jenis_barang/listjenisbarang"><i class="fa fa-circle-o"></i>Data Jenis Barang</a></li>
            <li><a href="<?=base_url();?>supplier/listsupplier"><i class="fa fa-circle-o"></i>Data Supplier</a></li>
            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i>
            <span>TRANSAKSI</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>pembelian/listpembelian"><i class="fa fa-circle-o text-red"></i>Data Pembelian</a></li>
            <li><a href="<?=base_url();?>penjualan/listpenjualan"><i class="fa fa-circle-o text-blue"></i>Data Penjualan</a></li>
            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>REPORT</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="<?=base_url();?>pembelian/dtp"><i class="fa fa-circle-o text-red"></i>Pembelian</a></li>
          <li class="active"><a href="<?=base_url();?>penjualan/dtpjual"><i class="fa fa-circle-o text-blue"></i> Penjualan</a></li>
            
          </ul>
        </li>
      </ul>
    </section>
  
