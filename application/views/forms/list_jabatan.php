

<table id="example2" class="table table-bordered table-hover">
                
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jabatan</h3>
            </div>
           <table width="100%" border="0" align="center">
      
    
  <table width="100%" align="center">
            <td colspan="5" align="left"><a href="<?=base_url();?>jabatan/inputjabatan" style="text-decoration:none;"><input type="submit" name="input" class="btn btn-info" value="Input Data"></a></td>
    <form action="<?=base_url()?>jabatan/listjabatan" method="POST">
<tr>
    <td>
      <?php
        if($this->session->flashdata('info') == true){
          echo $this->session->flashdata('info');
        }
        ?>
   </td>
</tr>
            <div class="box-tools" align="right">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="caridata" class="form-control pull-right" placeholder="Search" autocomplete="off">

                  <div class="input-group-btn">
                    <button type="submit" name="tombol_cari" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
           
          </form>
</table>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Jabatan</th>
                  <th>Nama Jabatan</th>
                  <th>Keterangan</th>
                  <td align="center" width="30%"><b>Aksi</td>
                </tr>
                <?php
            $data_posisi = $this->uri->segment(4);
            $no = $data_posisi;
            if (count($data_jabatan) > 0) {
             foreach ($data_jabatan as $data) {
             $no++;
              ?>
                </thead>
                <tbody>
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $data->kode_jabatan; ?></td>
                  <td><?= $data->nama_jabatan; ?></td>
                  <td><?= $data->keterangan; ?></td>
                  <td align="center">
        

<a href="<?=base_url();?>jabatan/detailjabatan/<?= $data->kode_jabatan; ?>">
<button type="button" style="background-color:#0F0" class="btn btn-info" name="detail" id="detail" ><i class="fa fa-file-text-o"></i> Detail</button></a>

  
<a href="<?=base_url();?>jabatan/editjabatan/<?= $data->kode_jabatan;?>">
<button type="button" name="edit" id="edit" class="btn btn-info" 
style="background-color:#09F"><i class="fa fa-pencil"></i> Edit</button></a>


<a href="<?=base_url();?>jabatan/deletejabatan/<?= $data->kode_jabatan;?>" 
onclick="return confirm('Yakin ingin hapus data?');" style="text-decoration:none;">
<button type="button" style="background-color: red" class="btn btn-info" name="delete" id="delete"><i class="fa fa-trash-o"></i> Delete</button></a>


      </td>
      </tr>
 <?php } ?>
  </table>
  <table border="0" align="center">
     <tr>
<td align="center"><br><br><?= $this->pagination->create_links();?></td>
    </tr>
    
    <?php } else {
     ?>
         <tr>
         <td colspan="7" align="center">--Tidak Ada Datanya--</td>
         </tr>
   <?php } ?>
     
                </td>
                </tr>
                
                </tbody>
                
              </table>
          </table>
     
  </div>
  <!-- /.content-wrapper -->


  
  
