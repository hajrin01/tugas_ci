

<table id="example2" class="table table-bordered table-hover">
                
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Penjualan</h3>
            </div>
           <table width="100%" border="0" align="center">

        <center><h3>Laporan Data Penjualan Dari Tanggal</h3></center>
        <center><h4><?=$tgl_awal;?> s/d <?=$tgl_akhir;?></h4></center>

<table width="100%" align="center">
  <tr>
    <td width="552"><div align="left"><a href="<?=base_url();?>penjualan/dtpjual">
      <input type="submit" class="btn btn-info" name="kembali" value="<<Kembali"></a>
      

    </div>
  <td width="552"><div align="right" ><a href="<?=base_url();?>penjualan/cetak/<?= $tgl_awal; ?>/<?= $tgl_akhir ;?>"><input type="submit" name="cetak" value="  Cetak PDF" class="btn btn-info"></a></div></td></td>
  </tr>

            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>ID Penjualan</th>
                    <th>Nomor Transaksi</th>
                    <th>Tanggal</th>
                    <th>Total Barang</th>
                    <th>Total Qty</th>
                    <th>Jumlah Nominal Penjualan</th>
                </tr>
                <?php
                $no = 0;
                 $total_keseluruhan = 0;

                  foreach ($data_penjualan_detail as $data) {
                  $no++;

              ?>
              </thead>
                <tbody>
                <tr>
                   <td><?=$no;?></td>
                  <td><?= $data->id_jual_h; ?></td>
                  <td><?= $data->no_transaksi; ?></td>
                  <td><?= $data->tanggal; ?></td>
                  <td><?= $data->total_barang; ?></td>
                  <td><?= $data->total_qty; ?></td>
                  <td>RP. <?= number_format($data->total_penjualan); ?></td>
              </tr>
              <?php 
                  //menghitung total
                  $total_keseluruhan+= $data->total_penjualan;
                }
                  ?>

      <tr>
        <td colspan="6" align="right"><b>Total Keseluruhan Penjualan</b></td>
        <td>Rp. <b><?=number_format($total_keseluruhan); ?></b></td>
    </tr>
    </table>
    
 </table>
       </tbody> 
              </table>
            