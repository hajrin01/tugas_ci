


<?php
foreach ($detail_supplier as $data) {
	$kode_supplier = $data->kode_supplier;
	$nama_supplier  = $data->nama_supplier;
	$alamat  = $data->alamat;
	$telp  = $data->telp;
	
}

?>
  
        <div class="box-header with-border">
              <h3 class="box-title">Edit Supplier</h3>
            </div>
          <center><div style="color: red"><?= validation_errors(); ?></div></center>
          <form action="<?=base_url()?>supplier/editsupplier/<?= $kode_supplier; ?>" method="POST">

              <div class="box-body">
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputEmail1">Kode Supplier</label>
                 <input type="text" class="form-control" name="kode_supplier" id="kode_supplier" maxlength="10" value ="<?=$kode_supplier;?>" value="<?= set_value('kode_supplier');?>" readonly >
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Nama Supplier</label>
                  <input type="text" class="form-control" name="nama_supplier" id="nama_supplier" value ="<?=$nama_supplier;?>" 
                  value="<?= set_value('nama_supplier');?>" autocomplete="off"/>
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Alamat</label>
                   <textarea name="alamat" class="form-control" id="alamat" cols="45" rows="3" value="<?= set_value('alamat');?>" /><?=$alamat;?></textarea>
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Telpon</label>
                  <input type="text" class="form-control" name="telp" id="telp" 
                  value ="<?=$telp;?>" value="<?= set_value('telp');?>" autocomplete="off"/>
                </div>
              

              <div class="box-footer">
                
                  <input type="submit" name="simpan" id="simpan" class="btn btn-info" value="simpan" style="background-color:#06F">

              <input type="submit" name="batal" id="batal" class="btn btn-info" 
              value="reset"
              style="background-color:#F00">

              <br></br>
              <a href="<?=base_url();?>supplier/listsupplier"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" class="btn btn-info" value="kembali ke menu sebelumnya" style="background-color:#0FF"></a>
              </div>
            </form>
