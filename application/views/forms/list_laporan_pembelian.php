 <table id="example2" class="table table-bordered table-hover">
                
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Pembelian</h3>
            </div>
           <table width="100%" border="0" align="center">

<center><h3>Laporan Data Pembelian Dari Tanggal</h3></center>
  <center><h4><?=$tgl_awal;?> s/d <?=$tgl_akhir;?></h4></center>
</br>
<table width="100%" align="center">
  <tr>
    <td width="552"><div align="left"><a href="<?=base_url();?>pembelian/dtp"><input type="submit" class="btn btn-info" name="kembali" value="<<Kembali"></a>
      

    </div>
  <td width="552"><div align="right" ><a href="<?=base_url();?>pembelian/cetak/<?= $tgl_awal; ?>/<?= $tgl_akhir ;?>"><input type="submit" name="cetak" value="  Cetak PDF" class="btn btn-info"></a></div></td></td>
  </tr>
<div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
    <tr>
    <th>No</th>
    <th>ID Pembelian</th>
    <th>Nomor Transaksi</th>
   	<th>Tanggal</th>
    <th>Total Barang</th>
    <th>Total Qty</th>
    <th>Jumlah Nominal Pembelian</th>
    
 </tr>
  <?php
  $no = 0;
   $total_keseluruhan = 0;

    foreach ($data_pembelian_detail as $data) {
		$no++;

?>
<tr>
   
    <td><?=$no;?></td>
    <td><?= $data->id_pembelian_h; ?></td>
	  <td><?= $data->no_transaksi; ?></td>
    <td><?= $data->tanggal; ?></td>
    <td><?= $data->total_barang; ?></td>
    <td><?= $data->total_qty; ?></td>
    <td>RP. <?= number_format($data->total_pembelian); ?></td>
</tr>
<?php 
		//menghitung total
		$total_keseluruhan+= $data->total_pembelian;
	}
?>
<tr>
        <td colspan="6" align="right"><b>Total Keseluruhan Pembelian</b></td>
        <td>Rp. <b> Rp.<?=number_format($total_keseluruhan); ?></b></td>
    </tr>
    </table>
 </table>
       </tbody> 
              </table>
  
