<table id="example2" class="table table-bordered table-hover">
           
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Karyawan</h3>
            </div>
<table width="100%" border="0" align="center">
      
    
  <table width="100%" align="center">
  <td colspan="5" align="left"><a href="<?=base_url();?>karyawan/inputkaryawan" 
    style="text-decoration:none;"><input type="submit" name="input" class="btn btn-info" 
    value="Input Data"></a></td>
  
  <form action="<?=base_url()?>karyawan/listkaryawan" method="POST">
<tr>
    <td>
      <?php
        if($this->session->flashdata('info') == true){
          echo $this->session->flashdata('info');
        }
        ?>
   </td>
</tr>
<div class="box-tools" align="right">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="caridata" class="form-control pull-right" placeholder="Search" autocomplete="off">

                  <div class="input-group-btn">
                    <button type="submit" name="tombol_cari" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>


          </form>
</table>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nik</th>
                  <th>Nama</th>
                  <th>Tempat Lahir</th>
                  <th>Telpon</th>
                  <th>Foto</th>
                  <td align="center" width="24%"><b>Aksi</td>
                </tr>
                <?php
        $data_posisi = $this->uri->segment(4);
        $no = $data_posisi;
        if (count($data_karyawan) > 0) {
        foreach ($data_karyawan as $data) {
        $no++;
        $foto_tampil = ($data->photo == "") ? "dafault.jpeg" : $data->photo;
        ?>
                </thead>
                <tbody>
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $data->nik; ?></td>
                  <td><?= $data->nama_lengkap; ?></td>
                  <td><?= $data->tempat_lahir; ?></td>
                  <td><?= $data->telp; ?></td>
                  <td>
        <?php
        if (!empty($data->photo)) {
         ?>
        <img src="<?=base_url();?>resources/fotokaryawan/<?= $data->photo;?>" width="100px">
         <?php
       
        } else {
        ?>
       <img src="<?=base_url();?>resources/fotokaryawan/default.jpg" width="100px">
        <?php }?>

        <td align="center">
<a href="<?=base_url();?>karyawan/detailkaryawan/<?= $data->nik; ?>">
<button type="button" style="background-color:#0F0" class="btn btn-info" name="detail" id="detail" ><i class="fa fa-file-text-o"></i> Detail</button></a>

  
<a href="<?=base_url();?>karyawan/editkaryawan/<?= $data->nik; ?>" 
style="text-decoration:none;">
<button type="button" name="edit" id="edit" class="btn btn-info" 
style="background-color:#09F"><i class="fa fa-pencil"></i> Edit</button></a>


<a href="<?=base_url();?>karyawan/deletekaryawan/<?= $data->nik; ?>" 
onclick="return confirm('Yakin ingin hapus data?');" style="text-decoration:none;">
<button type="button" style="background-color: red" class="btn btn-info" name="delete" id="delete"><i class="fa fa-trash-o"></i> Delete</button></a>

      </td>
      </tr>
  <?php } ?>

  </table>
  <table border="0" align="center" border="1">
     <tr>
<td align="center"><br><br><?= $this->pagination->create_links();?></td>
    </tr>
    <?php } else {
     ?>
         <tr>
         <td colspan="7" align="center">--Tidak Ada Datanya--</td>
         </tr>
   <?php } ?>
                </td>
                </tr>
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
      


