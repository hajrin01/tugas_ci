
<div class="box-header with-border">
              <h3 class="box-title">Input Penjualan Detail</h3>
            </div>
       <center><div style="color: red"><?= validation_errors(); ?></div></center>
    <form action="<?=base_url()?>penjualan/inputDetail/<?= $id_header; ?>" method="POST">
           
              <div class="box-body">
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputEmail1">Nama Barang</label>
                 <select id="kode_barang" class="form-control" name="kode_barang" 
                    value="<?= set_value('kode_barang');?>" />

                <?php foreach($data_barang as $data) { ?>
                <option value="<?= $data->kode_barang; ?>"><?= $data->nama_barang; ?></option>
                <?php }?>
            </select>
                </div>

                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Qty</label>
                    <input type="text" id="qty" class="form-control"name="qty" 
                    value="<?= set_value('qty');?>" autocomplete="off"/>
                </div>

              <div class="box-footer">
                
                  <input type="submit" value="Proses" class="btn btn-info" name="simpan" style="background-color:#06F" /> 
        
                <input type="submit" name="batal" id="batal" class="btn btn-info" value="reset" style="background-color:#F00"> 

                  <br></br>

              </div>
            </form>

<table id="example2" class="table table-bordered table-hover">
           
          
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
        <thead>
    <tr>
        <th>No</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Qty</th>
        <th>Harga</th>
        <th>Jumlah</th>
    </tr>
    <?php
    
        $no = 0;
        $total_hitung = 0;
        foreach ($data_penjualan_detail as $data) {
            $no++;
    ?>
    <tr>
        <td><?= $no; ?></td>
        <td><?= $data->kode_barang; ?></td>
        <td><?= $data->nama_barang; ?></td>
        <td><?= $data->qty; ?></td>
        <td>Rp. <?= number_format($data->harga); ?> ,-</td>
        <td>Rp. <?= number_format($data->jumlah); ?> ,-</td>
    </tr>
    <?php
        // hitung total
        $total_hitung += $data->jumlah;
        } 
    ?>
    <tr>
        <td colspan="5" align="right"><b>TOTAL</b></td>
        <td>Rp. <b><?= number_format($total_hitung); ?></b></td>
    </tr>
    </table>
                
        </tbody>
                
        </table>
    <tr align="center">
    
    <br/>
       <center> <a href="<?= base_url(); ?>penjualan/index">
          <input type="button" value="Kembali Ke Menu Sebelumnya" class="btn btn-info" name="kembali" />
        </a></center>
    </tr>
    </div>
            
      



